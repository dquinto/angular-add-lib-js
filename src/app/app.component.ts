import { Component, OnInit } from '@angular/core';
// declare const myTest: any;
declare const testmodule: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit {
  title = 'add-lib-js';

  ngOnInit(): void {
    // myTest();
    testmodule.testMethod()
    testmodule.testWorld("World")
  }
}
